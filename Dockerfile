#Docker base image : Alpine Linux with OpenJDK JDK
FROM maven:3-jdk-8-alpine AS build

VOLUME /tmp

##Set the working directory for RUN and ADD commands
WORKDIR /code

#Copy the SRC, LIB and pom.xml to WORKDIR
ADD pom.xml /code/pom.xml
ADD src /code/src

#Build the code
RUN mvn clean package

FROM gcr.io/distroless/java  

COPY --from=build /code/target/spring-gcp.jar app.jar

#Port the container listens on
EXPOSE 5000

ENTRYPOINT ["java","-Djava.security.egd=file:/dev/./urandom","-jar","/app.jar"]