package com.example.demo;

import java.util.Date;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/v1/greeting")
public class HelloWorld {

	@GetMapping("/name/{name}")
	public ResponseEntity<String> greetings(@PathVariable String name) {

		String message = "Hello ".concat(name.toUpperCase()).concat(". Today's date is ").concat(new Date().toString());
		return new ResponseEntity<>(message, HttpStatus.OK);
	}

}
